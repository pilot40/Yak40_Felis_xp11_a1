-- this is "black box" - MSRP system

-- collect sources
defineProperty("frame_time", globalPropertyf("sim/custom/xap/time/frame_time")) -- time for frames

defineProperty("date_days", globalPropertyi("sim/time/local_date_days"))
defineProperty("time_sec", globalPropertyf("sim/time/zulu_time_sec"))
defineProperty("flight_sec", globalPropertyf("sim/time/total_flight_time_sec"))

defineProperty("altitude", globalPropertyf("sim/cockpit2/gauges/indicators/altitude_ft_pilot"))
defineProperty("ias", globalPropertyf("sim/cockpit2/gauges/indicators/airspeed_kts_pilot"))
defineProperty("elevator", globalPropertyf("sim/custom/xap/control/pitch_anim")) -- animation of elevator
defineProperty("gforce", globalPropertyf("sim/flightmodel2/misc/gforce_normal"))
defineProperty("aileron", globalPropertyf("sim/cockpit2/controls/aileron_trim")) -- sim roll trimmer
defineProperty("roll", globalPropertyf("sim/custom/xap/gauges/roll_1")) -- roll angle
defineProperty("GMK_curse", globalPropertyf("sim/custom/xap/gauges/GMK_curse")) -- calculated course
defineProperty("rudder_trim", globalPropertyf("sim/cockpit2/controls/rudder_trim")) -- sim yaw trimmer
defineProperty("pitch_trim", globalPropertyf("sim/custom/xap/control/pitch_trim")) -- animation of elevator


-- select new filename for blackbox
local filename = panelDir .."/msrp_out/".. get(date_days) .. "_".. math.floor(get(time_sec)) .. ".txt"

local error_save = false

function savefile()
	local file = io.open(filename, "a")
	if file then
		--file:write(get(date_days),"\t") -- local date
		file:write(math.floor(get(time_sec)*100)*0.01,"\t") -- local time
		file:write(math.floor(get(flight_sec)*100)*0.01,"\t") -- flight time
		file:write(math.floor(get(altitude)*100 * 0.3048)*0.01,"\t") -- baro altitude from captain's altimeter, meters
		file:write(math.floor(get(ias)*100 * 1.852)*0.01,"\t") -- airspeed from captain's IAS, km/h
		file:write(math.floor(get(elevator)*100)*0.01,"\t") -- elevator position
		file:write(math.floor(get(gforce)*100)*0.01,"\t") -- G overload
		file:write(math.floor(get(aileron)*100)*0.01,"\t") -- aileron position	
		file:write(math.floor(get(roll)*10)*0.1,"\t") -- roll position
		file:write(math.floor(get(GMK_curse)*10)*0.1,"\t") -- gyro course
		file:write(math.floor(get(rudder_trim)*100)*0.01,"\t") -- rudder position			
		file:write(math.floor(get(pitch_trim)*100)*0.01,"\t") -- stab position		
		
		
		
		file:write("\n")
		error_save = false
	else 
		print("error writing file")
		error_save = true
	end
	file:close()
end

local time_counter = 0
local save_rate = 5 -- saves per second
function update()
	local passed = get(frame_time)
	time_counter = time_counter + passed
	if time_counter > 1 / save_rate and not error_save then
		--savefile()
		time_counter = 0
	end
end